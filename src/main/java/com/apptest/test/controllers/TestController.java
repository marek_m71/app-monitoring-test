package com.apptest.test.controllers;

import com.apptest.test.controllers.dto.RequestDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@Slf4j
public class TestController {

  @PostMapping("/sleep")
  public ResponseEntity<String> sleep(@RequestBody RequestDto requestDto) {
    log.info("Start request sleep with sleep: " + requestDto.getSecond());
    try {
      Thread.sleep(requestDto.getSecond() * 1000);
    } catch (InterruptedException ie) {
      Thread.currentThread().interrupt();
    }
    return new ResponseEntity<>("Response with delay: " + requestDto.getSecond() + " seconds", HttpStatus.OK);
  }

  @GetMapping("/test")
  public ResponseEntity<String> test() {
    log.info("Start request test with response OK");
    return new ResponseEntity<>("Test GET OK - OK Response", HttpStatus.OK);
  }

  @GetMapping("/test/error")
  public ResponseEntity<String> testBadGateway() {
    log.info("Start request test with response BadRequest");
    return new ResponseEntity<>("TEST GET OK - BadRequest Response", HttpStatus.BAD_REQUEST);
  }

}
