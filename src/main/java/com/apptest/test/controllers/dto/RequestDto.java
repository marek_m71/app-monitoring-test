package com.apptest.test.controllers.dto;

public class RequestDto {
  private Integer second;

  public Integer getSecond() {
    return second;
  }

  public void setSecond(Integer second) {
    this.second = second;
  }
}
