FROM openjdk:11-jdk

# The application's jar file
ARG JAR_FILE=target/test-0.0.1-SNAPSHOT.jar
ARG CONFIG=src/main/resources

# Add the application's jar to the container
ADD ${JAR_FILE} test.jar
ADD ${CONFIG} src/main/resources

EXPOSE 8200

ENTRYPOINT ["java","-jar","test.jar"]
